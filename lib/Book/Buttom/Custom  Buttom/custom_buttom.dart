import 'package:flutter/material.dart';

class CustomButtom extends StatelessWidget {
  final String? text;
  final Function()? onPressed;
  const CustomButtom({Key? key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return MaterialButton(
      height:50,
      minWidth: width,
      highlightColor: Colors.cyanAccent,
      onPressed: onPressed,
      color: Colors.indigoAccent,
      //shape: StadiumBorder(),
      child: Text(
        text!,
        style: TextStyle(color: Colors.white, fontSize: 17),
        //textAlign: TextAlign.center,
      ),
    );
  }
}
