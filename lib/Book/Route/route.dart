
import 'package:diploma_book/Book/Route/route_names.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Semester/semester_pages.dart';

class Routes{
  static  Route<dynamic> generateRoute(RouteSettings settings){

    switch (settings.name){
      case RouteName.SemesterPages:{
        return MaterialPageRoute(builder: (context)=>SemesterPages());
      }
      //
      // case RouteName.c1st:{
      //   return MaterialPageRoute(builder: (context)=>C1st(
      //   ));
      // }
      // case RouteName.c2st:{
      //   return MaterialPageRoute(builder: (context)=>C2st());
      // }
      default:
        return MaterialPageRoute(builder: (context){
          return Scaffold(
              body: Center(child: Text("No route defined"),)
          );
        });

    }
  }
}

