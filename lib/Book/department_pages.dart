import 'package:diploma_book/Book/Buttom/Custom%20%20Buttom/custom_buttom.dart';
import 'package:diploma_book/Book/probidan/D_probidhan_2016.dart';
import 'package:flutter/material.dart';
class DepartmentPages extends StatefulWidget {
  const DepartmentPages({Key? key}) : super(key: key);

  @override
  State<DepartmentPages> createState() => _DepartmentPagesState();
}

class _DepartmentPagesState extends State<DepartmentPages> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Probidhan of name".toUpperCase())),
      ),
      body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 5,),
              CustomButtom(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DProbidhan2016()));
                },
                text: "Probidhan_2016",
              ),
              SizedBox(height: 5,),
              CustomButtom(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DProbidhan2016()));
                },
                text: "Probidhan_2022",
              ),
            ],
          )),
    );
  }
}
